/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

import React, {
  Component,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
} from 'react-native';

var SignupHealth = require('./signupHealth.js')
var Login = require('./login.js')

class Dashboard extends Component {

  render() {
    return (
      <View style = {styles.container}> 
        <Image source={require('./resource/sunrise_background.png')} style={styles.backgroundImage}>
            <Text style={styles.title}>HealthApp</Text>
            <View style = {styles.container_title}>
              <Text style={styles.title_dashboard}>Do more__________.</Text>
            </View>
            <View style = {styles.container_main}> 
              <TouchableHighlight style={styles.bt_started} onPress={() => this.rowPressed()}>
                <Text style={styles.bt_started_lb}>Get Started</Text>
              </TouchableHighlight>
              <TouchableHighlight style={styles.bt_login} onPress={() => this.onLogin()}>
                <Text style = {styles.bt_login_lb}>LOG_IN</Text>
              </TouchableHighlight>
            </View>
        </Image>
      </View>      
    );
  }

  rowPressed()  {
    console.log('onGetStarted');
    this.props.navigator.push(
    {
      title:'Signup Detail',
      component : SignupHealth,
      leftButtonTitle:'Close',
      onLeftButtonPress:() => this.props.navigator.pop(),
    });
  }

  onLogin() {
    console.log('onLogin');
    this.props.navigator.push(
    {
      title:'Login',
      component:Login,
      leftButtonTitle:'Close',
      rightButtonTitle:'Done',
      onLeftButtonPress:() => this.props.navigator.pop(),
    });
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor:'transparent',
    alignSelf:'center',
  },
  container_title:{
    flex:1,
    alignItems:'center',
    padding:30,
    backgroundColor:'transparent',
    flexDirection:'row',
  },
  container_main:{
    flex:1,
    alignItems:'center',
    backgroundColor:'transparent',
    justifyContent:'flex-end',
    flexDirection:'column',
    padding:10,
    paddingBottom:40,
  },
  backgroundImage:{
    flex:1,
    alignItems: 'center',
    resizeMode:'cover',
  },
  title:{
    fontSize:20,
    textAlign:'center',
    color:'#656565',
    marginTop:20,
  },
  title_dashboard :{
    fontSize:30,
    textAlign:'center',
    color:'#FFFFFF',
    alignItems:'center',
    paddingBottom:40,
  },
  bt_started:{
    backgroundColor:'#FFFFFF',
    width:350,
    height:60,
    justifyContent:'center',
    borderRadius:5,
  },
  bt_started_lb:{
    fontSize:20,
    textAlign:'center'
  },
  bt_login:{
    backgroundColor:'transparent',
    height:20,
    width:80,
    marginTop:10,
    justifyContent:'center',
  },
  bt_login_lb:{
    fontSize:18,
    color:'#FFFFFF',
    textAlign:'center',
    alignSelf:'center'
  }
});

module.exports = Dashboard;