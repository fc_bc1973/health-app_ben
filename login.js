/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

import React, {
  Component,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  TextInput,
} from 'react-native';

class Login extends Component {

  render() {
    return (
      <View style = {styles.container}> 
        <View style={styles.email_view}>
          <Text style={styles.email_view_lb}>Email</Text>
          <TextInput style={styles.email_view_in} placeholder='example@example.com'/>
        </View>
        <View style={styles.password_view}>
          <Text style={styles.email_view_lb}>Password</Text>
          <TextInput style={styles.email_view_in} placeholder='At least 6 characters'/>
        </View>
        <View style={styles.forget_password_view}>
          <Text style={styles.forgetpassword_lb_s}>Forgot something?</Text>
          <Text style={styles.forgetpassword_lb_m}>Reset your password</Text>
        </View>
        <TouchableHighlight style={styles.login_facebook_view}>
          <Text style={styles.bt_facebook_lb}>Log In with Facebook</Text>
        </TouchableHighlight>
      </View>      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignSelf:'stretch',
    backgroundColor:'#e9e9e9', 
    flexDirection:'column',
    justifyContent: 'flex-start',
  },
  email_view:{
    flexDirection:'row',
    backgroundColor:'#ffffff',
    marginTop:80,
    alignItems:'center',
  },
  email_view_lb:{
    height:50,
    flex:1,
    marginRight:5,
    padding:10,
    fontSize:20,
    color:'#687175',
    alignItems:'center',
  },
  email_view_in:{
    height:50,
    flex:3,
    alignItems:'center',
  },
  line:{
    height:2,
    backgroundColor:'#e9e9e9',
    alignSelf:'stretch',
  },
  password_view:{
    flexDirection:'row',
    backgroundColor:'#ffffff',
    marginTop:1,
    alignItems:'center',
  },
  forget_password_view:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    padding:30,
  },
  forgetpassword_lb_s:{
    fontSize:16,
    color:'#697276',
    alignSelf:'center',
    marginRight:4,
  },
  forgetpassword_lb_m:{
    fontSize:20,
    color:'#000000',
    alignSelf:'center',
  },
  login_facebook_view:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#ffffff',
    padding:30,
  },
  bt_facebook_lb:{
    fontSize:22,
    color:'#3664a2',
    alignSelf:'center',
  }
});

module.exports = Login;