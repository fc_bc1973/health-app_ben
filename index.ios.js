/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
} from 'react-native';

var Dashboard = require('./dashboard.js');

class healthApp extends Component {
  render() {
    return (
      <React.NavigatorIOS
        style = {styles.container}
        initialRoute={{
          title:'Health App',
          component:Dashboard,
          passProps:{
            myProp:'Dashboard'
          },
        }} />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});

AppRegistry.registerComponent('healthApp', () => healthApp);
